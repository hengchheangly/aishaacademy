<?php 

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Fundrize_CPT' ) ) {
	class Fundrize_CPT {
		function __construct() {
			require_once dirname( __FILE__ ) . '/galleries.php';
			require_once dirname( __FILE__ ) . '/team.php';
			require_once dirname( __FILE__ ) . '/partner.php';

			add_filter( 'single_template', array( $this,'fundrize_single_gallery' ) );	    
			add_filter( 'archive_template', array( $this,'fundrize_archive_gallery' ) );	    
	    }

		function fundrize_single_gallery( $single_template ) {
			global $post;
			if ( $post->post_type == 'gallery' ) $single_template = dirname( __FILE__ ) . '/inc/single-gallery.php';
			return $single_template;
		}

		function fundrize_archive_gallery( $archive_template ) {
			global $post;
			if ( $post->post_type == 'gallery' ) $archive_template = dirname( __FILE__ ) . '/inc/archive-gallery.php';
			return $archive_template;
		}
	}
}
new Fundrize_CPT;