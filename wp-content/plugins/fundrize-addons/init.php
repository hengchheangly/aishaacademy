<?php 
/*
Plugin Name: Fundrize Addons 
Plugin URI: https://ninzio.com/
Description: Extend WPBakery page builder with Advanced Shortcodes.
Version: 1.2
Author: Ninzio Themes
Author URI: https://ninzio.com/
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Enqueue scripts
add_action( 'wp_enqueue_scripts', 'loadCssAndJs', 999999 );
function loadCssAndJs() {
	wp_enqueue_style( 'fundrize-flexslider', plugins_url('assets/flexslider.css', __FILE__), array(), '2.3.6' );
	wp_register_script( 'fundrize-flexslider', plugins_url('assets/flexslider.min.js', __FILE__), array('jquery'), '2.3.6', true );
	wp_enqueue_style( 'fundrize-owlcarousel', plugins_url('assets/owl.carousel.css', __FILE__), array(), '2.2.1' );
	wp_register_script( 'fundrize-owlcarousel', plugins_url('assets/owl.carousel.min.js', __FILE__), array('jquery'), '2.2.1', true );
	wp_enqueue_style( 'fundrize-cubeportfolio', plugins_url('assets/cubeportfolio.min.css', __FILE__), array(), '3.4.0' );
	wp_register_script( 'fundrize-cubeportfolio', plugins_url('assets/cubeportfolio.min.js', __FILE__), array('jquery'), '3.4.0', true );
	wp_register_script( 'fundrize-countto', plugins_url('assets/countto.js', __FILE__), array('jquery'), '1.0.0', true );
	wp_enqueue_style( 'fundrize-magnificpopup', plugins_url('assets/magnific.popup.css', __FILE__), array(), '1.0.0' );
	wp_register_script( 'fundrize-magnificpopup', plugins_url('assets/magnific.popup.min.js', __FILE__), array('jquery'), '1.0.0', true );
	wp_enqueue_style( 'fundrize-vegas', plugins_url('assets/vegas.css', __FILE__), array(), '2.3.1' );
	wp_register_script( 'fundrize-vegas', plugins_url('assets/vegas.js', __FILE__), array('jquery'), '2.3.1', true );
	wp_enqueue_style( 'fundrize-ytplayer', plugins_url('assets/ytplayer.css', __FILE__), array(), '3.0.2' );
	wp_register_script( 'fundrize-ytplayer', plugins_url('assets/ytplayer.js', __FILE__), array('jquery'), '3.0.2', true );
	wp_enqueue_script( 'fundrize-waypoints', plugins_url('assets/waypoints.js', __FILE__), array('jquery'), '2.0.4', true );
	wp_register_script( 'fundrize-fittext', plugins_url('assets/fittext.js', __FILE__), array('jquery'), '1.1.0', true );
	wp_register_script( 'fundrize-flowtype', plugins_url('assets/flowtype.js', __FILE__), array('jquery'), '1.3.0', true );
	wp_register_script( 'fundrize-typed', plugins_url('assets/typed.js', __FILE__), array('jquery'), '1.1.0', true );
	wp_enqueue_style( 'fundrize-shortcode', plugins_url('assets/shortcodes.css', __FILE__), array(), '1.0' );
	wp_enqueue_script( 'fundrize-shortcode', plugins_url('assets/shortcodes.js', __FILE__), array('jquery'), '1.0', true );
	wp_enqueue_script('google-maps-api', 'https://maps.googleapis.com/maps/api/js', array(), 'v3');
}

// Add image sizes
add_action( 'after_setup_theme', 'add_image_sizes' );
function add_image_sizes() {
	add_image_size( 'fundrize-square', 600, 600, true );
	add_image_size( 'fundrize-square2', 400, 400, true );
	add_image_size( 'fundrize-rectangle', 600, 500, true );
	add_image_size( 'fundrize-rectangle2', 600, 390, true );
	add_image_size( 'fundrize-medium-auto', 870, 9999 );
	add_image_size( 'fundrize-small-auto', 600, 9999 );
	add_image_size( 'fundrize-xsmall-auto', 480, 9999 );
}

// Map shortcodes to Visual Composer
require_once __DIR__ . '/vc-map.php';

// Include shortcodes files for Visual Composer
foreach ( glob( plugin_dir_path( __FILE__ ) . '/shortcodes/*.php' ) as $file ) {
	$filename = basename( $file );
	$tagname  = str_replace( '-', '_', pathinfo( $file, PATHINFO_FILENAME ) );

	add_shortcode( $tagname, function( $atts, $content = '' ) use( $file, $filename ) {
		ob_start();
		include $file;
		return ob_get_clean();
	} );
}

// Google API
require_once __DIR__ . '/google-api.php';

// Custom Post Type
require_once __DIR__ . '/cpt/init.php';

// Widgets
require_once __DIR__ . '/widgets/init.php';

?>