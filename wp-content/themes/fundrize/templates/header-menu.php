<?php
/**
 * Header / Menu
 *
 * @package fundrize
 * @version 3.6.8
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} ?>

<nav id="main-nav" class="main-nav">
	<?php
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'fallback_cb' => 'fundrize_menu_fallback',
		'container' => false
	) );
	?>
</nav>

<ul class="nav-extend active">
	<?php if ( fundrize_get_mod( 'header_search_icon', false ) ) : ?>
	<li class="ext"><?php get_search_form(); ?></li>
	<?php endif; ?>

	<?php if ( fundrize_get_mod( 'header_cart_icon', false ) && class_exists( 'woocommerce' ) ) : ?>
	<li class="ext"><a class="cart-info" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php echo esc_attr__( 'View your shopping cart', 'fundrize' ); ?>"><?php echo sprintf ( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), 'fundrize' ), WC()->cart->get_cart_contents_count() ); ?> <?php echo WC()->cart->get_cart_total(); ?></a></li>
	<?php endif; ?>
</ul>

<?php if ( fundrize_get_mod( 'header_cart_icon', false ) && class_exists( 'woocommerce' ) ) : ?>
<div class="nav-top-cart-wrapper">
    <a class="nav-cart-trigger" href="<?php echo esc_url( wc_get_cart_url() ) ?>">
    	<span class="cart-icon inf-icon-shop21">
        <?php if ( $items_count = WC()->cart->get_cart_contents_count() ): ?>
            <span class="shopping-cart-items-count"><?php echo esc_html( $items_count ) ?></span>
        <?php else: ?>
            <span class="shopping-cart-items-count">0</span>
        <?php endif ?>
        </span>
    </a>

    <div class="nav-shop-cart">
        <div class="widget_shopping_cart_content">
            <?php woocommerce_mini_cart() ?>
        </div>
    </div>
</div><!-- /.nav-top-cart-wrapper -->
<?php endif; ?>

<?php if ( fundrize_get_mod( 'header_search_icon', false ) ) : ?>
<div id="header-search">
	<a class="header-search-icon" href="#"><span class="inf-icon-magnifier9"></span></a>
	<form role="search" method="get" class="header-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<label class="screen-reader-text"><?php esc_html_e('Search for:', 'fundrize'); ?></label>
		<input type="text" value="<?php the_search_query(); ?>" name="s" class="header-search-field" placeholder="<?php esc_attr_e('Type and hit enter...', 'fundrize'); ?>" />
		<button type="submit" class="header-search-submit" title="<?php esc_attr_e('Search', 'fundrize'); ?>">
			<?php esc_html_e('Search', 'fundrize'); ?>
		</button>

		<!-- <input type="hidden" name="post_type" value="product" /> -->
		<input type="hidden" name="post_type" value="post" />
	</form>
</div><!-- /#header-search -->
<?php endif; ?>

