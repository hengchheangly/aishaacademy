<?php
/**
 * Accent color
 *
 * @package fundrize
 * @version 3.6.8
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Start Class
if ( ! class_exists( 'Fundrize_Accent_Color' ) ) {
	class Fundrize_Accent_Color {
		// Main constructor
		public function __construct() {
			add_filter( 'fundrize_footer_css', array( 'Fundrize_Accent_Color', 'generate' ), 1 );
		}

		// Generates arrays of elements to target
		private static function arrays( $return ) {
			// Color
			$texts = apply_filters( 'fundrize_accent_texts', array(
				'a', '.text-accent-color', '#site-logo .site-logo-text:hover',
				'.header-style-2 #top-bar .top-bar-content .content:before, .header-style-4 #top-bar .top-bar-content .content:before',
				'#site-header .header-search-icon:hover',
				'#main-nav > ul > li > a:hover',
				'#main-nav > ul > li.current-menu-item > a',
				'.nav-top-cart-wrapper .nav-shop-cart ul li a.remove',
				'.nav-top-cart-wrapper .nav-shop-cart ul li a:hover',
				'#site-header .fundrize-info .info-c > .title',
				'#featured-title #breadcrumbs a:hover',
				'#featured-title #breadcrumbs .breadcrumb-trail > a:before, #featured-title #breadcrumbs .breadcrumb-trail > span:before',
				'.hentry .post-title a:hover', '.hentry .post-meta a:hover',
				'#footer-widgets .widget.widget_search .search-form .search-submit:before',
				'.widget.widget_categories ul li a:hover',
				'.widget.widget_meta ul li a:hover',
				'.widget.widget_pages ul li a:hover',
				'.widget.widget_archive ul li a:hover',
				'.widget.widget_recent_entries ul li a:hover',
				'.widget.widget_recent_comments ul li a:hover',
				'#sidebar .widget.widget_calendar caption',
				'#footer-widgets .widget.widget_calendar caption',
				'#sidebar .widget.widget_links ul li a:hover',
				'#footer-widgets .widget.widget_links ul li a:hover',
				'#sidebar .widget.widget_recent_posts h3 a:hover',
				'#footer-widgets .widget.widget_recent_posts h3 a:hover',
				'#sidebar .widget.widget_calendar tbody #today a',
				'#footer-widgets .widget.widget_categories ul li a:hover',
				'#footer-widgets .widget.widget_meta ul li a:hover',
				'#footer-widgets .widget.widget_pages ul li a:hover',
				'#footer-widgets .widget.widget_archive ul li a:hover',
				'#footer-widgets .widget.widget_recent_entries ul li a:hover',
				'#footer-widgets .widget.widget_recent_comments ul li a:hover',
				'#sidebar .widget.widget.widget_information ul li i',
				'#footer-widgets .widget.widget.widget_information ul li i',
				'#sidebar .widget.widget_recent_posts .recent-news .thumb.icon, #footer-widgets .widget.widget_recent_posts .recent-news .thumb.icon',
				'.widget.widget_nav_menu ul li a:hover',
				'.widget.widget_nav_menu .menu > li.current-menu-item > a',
				'.widget.widget_nav_menu .menu > li.current-menu-item',
				'.hentry .post-author .name',
				'.hentry .post-related .post-item h4 a:hover',
				'.comment-author',
				'#bottom ul.bottom-nav > li.current-menu-item > a',
				// shortcodes
				'.fundrize-button.outline.ol-accent',
				'.fundrize-divider.has-icon .icon-wrap > span.accent',
				'.fundrize-list .icon.accent',
				'.fundrize-icon.background .icon.accent',
				 '.fundrize-icon-box.accent-outline .icon-wrap', '.fundrize-icon-box.grey-outline .icon-wrap', '.fundrize-icon-box .heading a:hover', '.fundrize-icon-box.simple .icon-wrap',
				 '.fundrize-image-box .item .title a:hover',
				 '.fundrize-news .news-item .text-wrap .title a:hover',
				 '.fundrize-news-simple .text-wrap .title a:hover',
				 '.fundrize-counter .icon-wrap .icon.accent',
				 '.fundrize-counter .number-wrap .number.accent',
				 '.fundrize-accordions .accordion-item .accordion-heading:hover',
				 '.fundrize-accordions.style-1 .accordion-item .accordion-heading:after',
				 '.fundrize-causes .campaign .campaign-donation-stats .amount',
				 '.fundrize-causes .campaign .text-wrap .title a:hover',
				 '.single-figure .figure .amount', '.single-figure .figure .days-left',
				 '#gallery-filter .cbp-filter-item:hover', '.gallery-box .effect-default .text h2 a:hover',
				 '.fundrize-subscribe.style-2 .text-wrap:before',
				 '.fundrize-action-box.has-icon .heading-wrap > .text-wrap > .icon.accent',
				 // Woocommerce
				 '.products li .price',
				 '.products li h2:hover, .products li .product-info .add_to_cart_button:hover, .products li .added_to_cart:hover',
				 '.woo-single-post-class .summary .price',
				 '.woocommerce-page .shop_table.cart .product-name a:hover',
				 '.woocommerce-page .woocommerce-message .button, .woocommerce-page .woocommerce-info .button, .woocommerce-page .woocommerce-error .button',
				 '.woocommerce-page .product_list_widget .product-title:hover, .woocommerce-page .widget_recent_reviews .product_list_widget a:hover, .woocommerce-page .product_list_widget .mini_cart_item a:hover',
				 '.woocommerce-page .widget_product_categories ul li a:hover'
			) );

			// Background color
			$backgrounds = apply_filters( 'fundrize_accent_backgrounds', array(
				'blockquote:before',
				'.header-style-1 #top-bar .top-bar-socials .icons a:hover, .header-style-2 #top-bar .top-bar-socials .icons a:hover, .header-style-3 #top-bar .top-bar-socials .icons a:hover, .header-style-4 #top-bar .top-bar-socials .icons a:hover',
				'#site-header .site-navigation-wrap',
				'#featured-title .featured-title-heading:before',
				'.post-media .slick-prev:hover, .post-media .slick-next:hover', '.post-media .slick-dots li.slick-active button',
				'.header-style-4 #site-header .header-aside-btn a',
				'.fundrize-pagination ul li a.page-numbers:hover',
				'.woocommerce-pagination .page-numbers li .page-numbers:hover',
				'.fundrize-pagination ul li .page-numbers.current',
				'.woocommerce-pagination .page-numbers li .page-numbers.current',
				'.hentry .post-share a:hover:after',
				'.comments-area .comments-title:after',
				'.comments-area .comment-reply-title:after',
				'.comment-reply:after',
				'#scroll-top:hover:before',
				'.widget.widget_categories ul li a:before, .widget.widget_meta ul li a:before, .widget.widget_pages ul li a:before, .widget.widget_archive ul li a:before',
				'#sidebar .widget.widget_socials .socials a:hover, #footer-widgets .widget.widget_socials .socials a:hover',
				'.button-widget a:hover',
				'#sidebar .widget.widget_tag_cloud .tagcloud a:hover:after',
				'#footer-widgets .widget.widget_tag_cloud .tagcloud a:hover:after',
				'.widget_product_tag_cloud .tagcloud a:hover:after',
				'#footer-widgets .widget .widget-title > span:after',
				'#footer-widgets .widget.widget_recent_posts .recent-news .thumb.icon',
				'.post-media > .post-cat a',
				'.hentry .post-related .post-thumb .post-cat-related a',
				'.hentry .post-tags a:hover',
				'.nav-top-cart-wrapper .nav-shop-cart .buttons > a:first-child',
				'.nav-top-cart-wrapper .shopping-cart-items-count',
				'#bottom ul.bottom-nav > li.current-menu-item > a:before',
				'#charitable-donation-form .donation-amounts .donation-amount.custom-donation-amount.selected input, #charitable-donation-form .donation-amounts .donation-amount.suggested-donation-amount.selected .amount',
				// shortcodes
				'.fundrize-button.accent',
				'.fundrize-button.outline:hover',
				'.fundrize-button.dark:hover',
				'.fundrize-button.light:hover',
				'.fundrize-button.very-light:hover',
				'.fundrize-button.outline.dark:hover',
				'.fundrize-button.outline.light:hover',
				'.fundrize-button.outline.very-light:hover',
				'.fundrize-headings .sep',
				'.fundrize-counter .sep.accent',
				'.fundrize-icon.background .icon.bg-accent',
				'.fundrize-icon-box .btn .simple-link:after',
				'.fundrize-icon-box.accent-bg .icon-wrap', '.fundrize-icon-box.accent-bg .icon-wrap:after', '.fundrize-icon-box.grey-bg:hover .icon-wrap', '.fundrize-icon-box.grey-bg .icon-wrap:after', '.fundrize-icon-box.accent-outline:hover .icon-wrap', '.fundrize-icon-box.accent-outline .icon-wrap:after', '.fundrize-icon-box.grey-outline:hover .icon-wrap', '.fundrize-icon-box.grey-outline .icon-wrap:after',
				'.fundrize-image-box .item .simple-link:after',
				'.fundrize-news .news-item .simple-link:after',
				'#gallery-filter .cbp-filter-item.cbp-filter-item-active:before',
				'.has-arrows .cbp-nav-next',
				'.has-arrows .cbp-nav-prev',
				'.bullet-style-1 .cbp-nav-pagination-active', '.bullet-style-2 .cbp-nav-pagination-active ',
				'.fundrize-lines .line-1',
				'.fundrize-navbar .menu > li.current-nav-item > a',
				'.fundrize-progress .progress-animate.accent', '.fundrize-progress.pstyle-2 .perc > span',
				'.fundrize-accordions.style-1 .accordion-item.active .accordion-heading',
				'.fundrize-socials a:hover', '.fundrize-socials.style-2 a:hover',
				'.fundrize-team .socials li a:hover',
				'.fundrize-price-table .price-table-price.accent',
				'.fundrize-menu-list .value',
				'.owl-theme .owl-dots .owl-dot.active span',
				'.fundrize-causes .fundrize-progress .perc > span', '.fundrize-causes .campaign .thumb-wrap .campaign-donation .dnt-button',
				'.fundrize-causes .fundrize-progress .progress-animate',
				'.single-figure .fundrize-progress .progress-animate',
				'.single-title .campaign-donation .dnt-button', '.single-figure .fundrize-progress .perc > span',
				'.fundrize-subscribe.bg-accent',
				'.fundrize-subscribe .mc4wp-form .submit-wrap input',
				'.fundrize-tabs.style-2 .tab-title .item-title.active',
				'.fundrize-tabs.style-3 .tab-title .item-title.active',
				'.fundrize-action-box.accent',
				// woocemmerce
				'.product .onsale',
				'.products li .product-info .add_to_cart_button:after, .products li .product-info .product_type_variable:after',
				'.woocommerce-page .wc-proceed-to-checkout .button', '.woocommerce-page #payment #place_order',
				'.woocommerce-page .widget_price_filter .ui-slider .ui-slider-range',
				'.woocommerce-page .widget_shopping_cart .wc-forward:hover, .woocommerce-page .widget_shopping_cart .wc-forward.checkout:hover',
				'.woocommerce-page .widget_price_filter .price_slider_amount .button:hover'
			) );

			// Border color
			$borders = apply_filters( 'fundrize_accent_borders', array(
				'.animsition-loading:after',
				'.fundrize-pagination ul li a.page-numbers:hover',
				'.woocommerce-pagination .page-numbers li .page-numbers:hover',
				'.fundrize-pagination ul li .page-numbers.current',
				'.woocommerce-pagination .page-numbers li .page-numbers.current',
				'#sidebar .widget.widget_socials .socials a:hover, #footer-widgets .widget.widget_socials .socials a:hover',
				'.button-widget a:hover',
				'#sidebar .widget.widget_tag_cloud .tagcloud a:hover:after',
				'#footer-widgets .widget.widget_tag_cloud .tagcloud a:hover:after',
				'.widget_product_tag_cloud .tagcloud a:hover:after',
				'.hentry .post-link a',
				'.hentry .post-tags a:hover',
				'.owl-theme .owl-dots .owl-dot.active span',
				// shortcodes
				'.fundrize-divider.divider-solid.accent',
				'.divider-icon-before.accent, .divider-icon-after.accent, .fundrize-divider.has-icon .divider-double.accent',
				'.fundrize-button.outline.ol-accent',
				'.fundrize-button.outline.dark:hover',
				'.fundrize-button.outline.light:hover',
				'.fundrize-button.outline.very-light:hover',
				'.fundrize-icon.outline .icon', 
				'.fundrize-icon-box.grey-bg:hover .icon-wrap:after', '.fundrize-icon-box.accent-outline .icon-wrap', '.fundrize-icon-box.grey-outline:hover .icon-wrap',
				'.fundrize-navbar .menu > li.current-nav-item > a',
				'.fundrize-galleries #fundrize-carousel .slides > li:hover:after',
				'.fundrize-progress.style-2.pstyle-1 .perc > span:after',
				'.fundrize-causes .fundrize-progress .perc > span:after',
				'.single-figure .fundrize-progress .perc > span:after',
				'.fundrize-tabs.style-1 .tab-title .item-title.active > span',
				'.fundrize-tabs.style-2 .tab-title .item-title.active > span',
				'.fundrize-tabs.style-4 .tab-title .item-title.active > span',
				// woocommerce
				'.woo-single-post-class .woocommerce-tabs ul li.active > a',
				'.woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle',
				'.woocommerce-page .widget_shopping_cart .wc-forward',
				'.woocommerce-page .widget_shopping_cart .wc-forward:hover, .woocommerce-page .widget_shopping_cart .wc-forward.checkout:hover',
				'.woocommerce-page .widget_price_filter .price_slider_amount .button:hover'
			) );

			// Return array
			if ( 'texts' == $return ) {
				return $texts;
			} elseif ( 'backgrounds' == $return ) {
				return $backgrounds;
			} elseif ( 'borders' == $return ) {
				return $borders;
			}
		}

		// Generates the CSS output
		public static function generate( $output ) {

			// Get custom accent
			$default_accent = '#f57223';
			$custom_accent  = fundrize_get_mod( 'accent_color' );

			// Return if accent color is empty or equal to default
			if ( ! $custom_accent || ( $default_accent == $custom_accent ) )
				return $output;

			// Define css var
			$css = '';

			// Get arrays
			$texts       = self::arrays( 'texts' );
			$backgrounds = self::arrays( 'backgrounds' );
			$borders     = self::arrays( 'borders' );

			// Texts
			if ( ! empty( $texts ) )
				$css .= implode( ',', $texts ) .'{color:'. $custom_accent .';}';

			// Backgrounds
			if ( ! empty( $backgrounds ) )
				$css .= implode( ',', $backgrounds ) .'{background-color:'. $custom_accent .';}';

			// Borders
			if ( ! empty( $borders ) ) {
				foreach ( $borders as $key => $val ) {
					if ( is_array( $val ) ) {
						$css .= $key .'{';
						foreach ( $val as $key => $val ) {
							$css .= 'border-'. $val .'-color:'. $custom_accent .';';
						}
						$css .= '}'; 
					} else {
						$css .= $val .'{border-color:'. $custom_accent .';}';
					}
				}
			}
			
			// Return CSS
			if ( ! empty( $css ) )
				$output .= $css;

			// Return output css
			return $output;
		}
	}
}

new Fundrize_Accent_Color();